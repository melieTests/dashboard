import React from 'react'
import { Box, Typography } from '@mui/material'
import useMediaQuery from '@mui/material/useMediaQuery'

const lightGreen = 'rgba(187,224,150,255)'
const green = 'rgba(135,205,49,255)'
const blue = 'rgba(79,223,193,255)'
const orange = 'rgba(231,152,69,255)'
const darkBlue = 'rgba(51,60,75,255)'
const grey = 'rgba(240,241,237,255)'

const Header = () => {
	return (
		<Box display="flex" height="80px">
			<Box p="10px" display="flex" gap="1rem" backgroundColor={darkBlue} borderRadius="0 0 4px 0" textAlign="center">
				<Box display="flex" alignItems="center" height="100%">
					<Typography variant="h3">Product</Typography>
				</Box>
				<Box color={lightGreen}>
					<Typography variant="h5">TODO</Typography>
					<Typography variant="h2" fontWeight="bold">
						44
					</Typography>
				</Box>
				<Box color={green}>
					<Typography variant="h5">WIP</Typography>
					<Typography variant="h2" fontWeight="bold">
						11
					</Typography>
				</Box>
				<Box color={blue}>
					<Typography variant="h5">DONE</Typography>
					<Typography variant="h2" fontWeight="bold">
						325
					</Typography>
				</Box>
				<Box color={orange}>
					<Typography variant="h5">BLOCKED</Typography>
					<Typography variant="h2" fontWeight="bold">
						6
					</Typography>
				</Box>
			</Box>
			<Box
				p="10px"
				backgroundColor={grey}
				borderRadius="0 0 4px 0"
				color={darkBlue}
				display="flex"
				alignItems="center"
				gap="1rem"
			>
				<Typography fontSize="20px" lineHeight="1.2">
					TOTAL
					<br /> PROGRESS
				</Typography>
				<Box display="flex">
					<Typography fontWeight="bold" fontSize="50px">
						86
					</Typography>
					<Typography fontSize="24px">
						<sup xs={{ fontSize: '12px' }}>%</sup>
					</Typography>
				</Box>
			</Box>
		</Box>
	)
}

export default Header
