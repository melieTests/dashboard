import React from 'react'
import { Box, Typography, Divider } from '@mui/material'
import useMediaQuery from '@mui/material/useMediaQuery'
import Header from './header'

import SettingsOutlinedIcon from '@mui/icons-material/SettingsOutlined'
import FiberManualRecordIcon from '@mui/icons-material/FiberManualRecord'
import InsightsIcon from '@mui/icons-material/Insights'
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined'
import Grid3x3OutlinedIcon from '@mui/icons-material/Grid3x3Outlined'
import CropFreeOutlinedIcon from '@mui/icons-material/CropFreeOutlined'

import DonutChart from '../../components/charts/donutChart'
import Gauge from '../../components/charts/gauge'
import SimpleColumnChart from '../../components/charts/simpleColumnChart'
import StackedColumnChart from '../../components/charts/stackedColumnChart'
import RadialBarChart from '../../components/charts/radialBarChart'

const green = 'rgba(135,205,49,255)'
const orange = 'rgba(255,196,76,255)'
const red = 'rgba(252,94,96,255)'
const darkBlue = 'rgba(51,60,75,255)'
const grey = 'rgba(240,241,237,255)'
const white = 'rgba(250,250,250,255)'
const regularGrey = 'rgba(197,197,197,255)'

const Card = ({ children, gridColumn, flex }) => {
	return (
		<Box
			p="10px"
			gridColumn={gridColumn}
			backgroundColor="#fff"
			borderRadius="4px"
			sx={{ border: `2px solid ${grey}` }}
			flex={flex}
			display="flex"
			flexDirection="column"
			justifyContent="space-between"
			gap="5px"
		>
			{children}
		</Box>
	)
}

const IconBox = ({ icon }) => {
	return (
		<Box
			display="flex"
			alignItems="center"
			borderRadius="4px"
			p="2px"
			sx={{ '& .MuiSvgIcon-root': { fontSize: '15px' }, border: `1px solid ${grey}`, cursor: 'pointer' }}
		>
			{icon}
		</Box>
	)
}

const TimeBox = ({ d, h, m, status, big }) => {
	return (
		<Box display="flex" gap="6px" alignItems="end">
			<Box display="flex" gap="2px" alignItems="end">
				<Typography fontSize={big ? '40px' : '18px'} lineHeight={big ? '.9' : '1.2'}>
					{d}
				</Typography>
				<Typography fontWeight="bold" fontSize="10px" color={regularGrey}>
					D
				</Typography>
			</Box>
			<Box display="flex" gap="2px" alignItems="end">
				<Typography fontSize={big ? '40px' : '18px'} lineHeight={big ? '.9' : '1.2'}>
					{h}
				</Typography>
				<Typography fontWeight="bold" fontSize="10px" color={regularGrey}>
					H
				</Typography>
			</Box>
			<Box display="flex" gap="2px" alignItems="end">
				<Typography fontSize={big ? '40px' : '18px'} lineHeight={big ? '.9' : '1.2'}>
					{m}
				</Typography>
				<Typography fontWeight="bold" fontSize="10px" color={regularGrey}>
					M
				</Typography>
			</Box>
			<FiberManualRecordIcon
				fontSize="small"
				sx={{ color: status === 'inProgress' ? orange : status === 'clear' ? green : red }}
			/>
		</Box>
	)
}

const SingleDataInfoBox = ({ number, status, unite, flex }) => {
	return (
		<Box display="flex" flex={flex} alignItems="center" gap="5px">
			<Typography fontSize="40px" lineHeight="1.2">
				{number}
			</Typography>
			<Box display="flex" flexDirection="column" alignItems="center">
				<FiberManualRecordIcon
					fontSize="small"
					sx={{
						color:
							status === 'inProgress'
								? orange
								: status === 'clear'
								? green
								: status === 'warning'
								? red
								: 'transparent',
					}}
				/>
				<Typography sx={{ color: regularGrey }}>{unite}</Typography>
			</Box>
		</Box>
	)
}

const Dash = () => {
	const isNonMobile = useMediaQuery('(min-width:600px)')
	const isNonTablet = useMediaQuery('(min-width:1000px)')
	const isNonMediumDevice = useMediaQuery('(min-width:1400px)')
	const column = isNonMediumDevice ? 'span 3' : isNonTablet ? 'span 4' : isNonMobile ? 'span 6' : 'span 12'

	return (
		<Box backgroundColor={white} minHeight="100%">
			<Header />
			<Box m="20px" color={darkBlue} display="grid" gridTemplateColumns="repeat(12, 1fr)" gap="20px" height="100%">
				{/* ROW 1 */}
				<Card gridColumn={column}>
					<Box>
						<Box display="flex" justifyContent="space-between">
							<Typography fontWeight="bold" fontSize="16px">
								FLOW TIME
							</Typography>
							<IconBox icon={<SettingsOutlinedIcon />} />
						</Box>
						<Typography fontSize="12px" color={regularGrey}>
							All
						</Typography>
					</Box>
					<Box flex="1" display="flex" flexDirection="column" justifyContent="space-evenly">
						<Box padding=".5rem 0">
							<Typography p="0 0 5px 0" fontSize="13px" fontWeight="500">
								Lead Time
							</Typography>
							<Box display="flex" justifyContent="space-between">
								<TimeBox d={11} h={23} m={13} status="inProgress" />
								<IconBox icon={<InsightsIcon />} />
							</Box>
						</Box>
						<Divider sx={{ borderColor: grey, borderWidth: '1px' }} />
						<Box padding=".5rem 0">
							<Typography p="0 0 5px 0" fontSize="13px" fontWeight="500">
								Reaction Time
							</Typography>
							<Box display="flex" justifyContent="space-between">
								<TimeBox d={11} h={23} m={13} status="inProgress" />
								<IconBox icon={<InsightsIcon />} />
							</Box>
						</Box>
						<Divider sx={{ borderColor: grey, borderWidth: '1px' }} />
						<Box padding=".5rem 0">
							<Typography p="0 0 5px 0" fontSize="13px" fontWeight="500">
								Cycle Time
							</Typography>
							<Box display="flex" justifyContent="space-between">
								<TimeBox d={11} h={23} m={13} status="warning" />
								<IconBox icon={<InsightsIcon />} />
							</Box>
						</Box>
					</Box>
					<Box display="flex" justifyContent="end">
						<InfoOutlinedIcon sx={{ color: regularGrey }} fontSize="small" />
					</Box>
				</Card>
				<Box gridColumn={column} display="flex" flexDirection="column" gap="20px">
					<Card flex="1">
						<Box>
							<Box display="flex" justifyContent="space-between">
								<Typography fontWeight="bold" fontSize="16px">
									EFFICIENCY
								</Typography>
								<Box display="flex" gap="5px">
									<IconBox icon={<SettingsOutlinedIcon />} />
									<IconBox icon={<InsightsIcon />} />
									<IconBox icon={<Grid3x3OutlinedIcon />} />
								</Box>
							</Box>
							<Typography fontSize="12px" color={regularGrey}>
								Story
							</Typography>
						</Box>
						<Box display="flex" justifyContent="space-around">
							<SingleDataInfoBox flex="1" number={27} status="inProgress" unite="%" />
							<DonutChart />
						</Box>
						<Box display="flex" justifyContent="end">
							<InfoOutlinedIcon sx={{ color: regularGrey }} fontSize="small" />
						</Box>
					</Card>
					<Card flex="1">
						<Box>
							<Box display="flex" justifyContent="space-between">
								<Typography fontWeight="bold" fontSize="16px">
									AGING
								</Typography>
								<Box display="flex" gap="5px">
									<IconBox icon={<SettingsOutlinedIcon />} />
									<IconBox icon={<InsightsIcon />} />
								</Box>
							</Box>
							<Typography fontSize="12px" color={regularGrey}>
								Issue
							</Typography>
						</Box>
						<TimeBox d={11} h={23} m={13} status="clear" big />
						<Box display="flex" justifyContent="end">
							<InfoOutlinedIcon sx={{ color: regularGrey }} fontSize="small" />
						</Box>
					</Card>
				</Box>
				<Box gridColumn={column} display="flex" flexDirection="column" gap="20px">
					<Card flex="1">
						<Box>
							<Box display="flex" justifyContent="space-between">
								<Typography fontWeight="bold" fontSize="16px">
									SCRUM PREDICT.
								</Typography>
								<Box display="flex" gap="5px">
									<IconBox icon={<SettingsOutlinedIcon />} />
								</Box>
							</Box>
							<Typography fontSize="12px" color={regularGrey}>
								Issue
							</Typography>
						</Box>
						<Box display="flex" justifyContent="space-between" gap="20px">
							<SingleDataInfoBox flex="1" number={75} status="clear" unite="%" />
							<Box flex="1">
								<Box display="flex" justifyContent="space-between">
									<Typography sx={{ color: regularGrey }}>COMMITMENT</Typography>
									<Typography>00.00</Typography>
								</Box>
								<Box display="flex" justifyContent="space-between">
									<Typography sx={{ color: regularGrey }}>VELOCITY</Typography>
									<Typography>00.00</Typography>
								</Box>
							</Box>
						</Box>
						<Box display="flex" justifyContent="end">
							<InfoOutlinedIcon sx={{ color: regularGrey }} fontSize="small" />
						</Box>
					</Card>
					<Card flex="1">
						<Box display="flex" justifyContent="space-between">
							<Typography fontWeight="bold" fontSize="16px">
								DEFECT
							</Typography>
							<Box display="flex" gap="5px">
								<IconBox icon={<CropFreeOutlinedIcon />} />
							</Box>
						</Box>
						<Box display="flex" justifyContent="space-around" gap="20px" flexWrap="wrap">
							<SingleDataInfoBox number={0.57} unite="BUG/ISSUE" />
							<Gauge />
						</Box>
						<Box display="flex" justifyContent="end">
							<InfoOutlinedIcon sx={{ color: regularGrey }} fontSize="small" />
						</Box>
					</Card>
				</Box>
				<Card gridColumn={column}>
					<Box display="flex" justifyContent="space-between">
						<Typography fontWeight="bold" fontSize="16px">
							THROUGHPUT
						</Typography>
						<Box display="flex" gap="5px">
							<IconBox icon={<CropFreeOutlinedIcon />} />
							<IconBox icon={<InsightsIcon />} />
						</Box>
					</Box>
					<Box display="flex" flexDirection="column" gap="20px">
						<SingleDataInfoBox number={1.02} status="clear" unite="ISSUE/DAY" />
						<SimpleColumnChart name="short" />
					</Box>
					<Box display="flex" justifyContent="end">
						<InfoOutlinedIcon sx={{ color: regularGrey }} fontSize="small" />
					</Box>
				</Card>
				{/* ROW 2 */}
				<Card gridColumn={column}>
					<Typography fontWeight="bold" fontSize="16px">
						BURNUP
					</Typography>
					<Box display="flex" justifyContent="end">
						<InfoOutlinedIcon sx={{ color: regularGrey }} fontSize="small" />
					</Box>
				</Card>
				<Card gridColumn={column}>
					<Box>
						<Box display="flex" justifyContent="space-between">
							<Typography fontWeight="bold" fontSize="16px">
								BACKLOG PRIORITY
							</Typography>
							<Box display="flex" gap="5px">
								<IconBox icon={<SettingsOutlinedIcon />} />
								<IconBox icon={<CropFreeOutlinedIcon />} />
							</Box>
						</Box>
						<Typography fontSize="12px" color={regularGrey}>
							Issue
						</Typography>
					</Box>
					<StackedColumnChart name="stacked" />
					<Box display="flex" justifyContent="end">
						<InfoOutlinedIcon sx={{ color: regularGrey }} fontSize="small" />
					</Box>
				</Card>
				<Card gridColumn={column}>
					<Box>
						<Box display="flex" justifyContent="space-between">
							<Typography fontWeight="bold" fontSize="16px">
								FLOW TIME BY
							</Typography>
							<Box display="flex" gap="5px">
								<IconBox icon={<SettingsOutlinedIcon />} />
								<IconBox icon={<CropFreeOutlinedIcon />} />
							</Box>
						</Box>
						<Typography fontSize="12px" color={regularGrey}>
							Issue
						</Typography>
					</Box>
					{/* <SimpleColumnChart name="long" long /> */}
					<StackedColumnChart name="long" long />
					<Box display="flex" justifyContent="end">
						<InfoOutlinedIcon sx={{ color: regularGrey }} fontSize="small" />
					</Box>
				</Card>
				<Card gridColumn={column}>
					<Box>
						<Box display="flex" justifyContent="space-between">
							<Typography fontWeight="bold" fontSize="16px">
								ISSUE BACKLOG
							</Typography>
							<IconBox icon={<CropFreeOutlinedIcon />} />
						</Box>
					</Box>
					<RadialBarChart name="radiale" />
					<Box display="flex" justifyContent="end">
						<InfoOutlinedIcon sx={{ color: regularGrey }} fontSize="small" />
					</Box>
				</Card>
			</Box>
		</Box>
	)
}

export default Dash
